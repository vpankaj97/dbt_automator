from src.SnowflakeExecuter import SnowflakeExecuter
from threading import Thread
from time import sleep


if __name__ == '__main__':
    snowflakeobj = SnowflakeExecuter()

    while True:
        print("starting job" )
        snowflakeobj.execute_snowflake_query()
        print("sleeping state")
        sleep(400)

