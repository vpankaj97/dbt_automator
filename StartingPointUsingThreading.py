from threading import Thread
from time import sleep
from src.DbtJobRunStatus import run
from src.SnowflakeExecuter import SnowflakeExecuter
 
# function to create threads
def threaded_function():
    #for i in range(arg):
    while True:
        print("starting")
        SnowflakeExecuter().execute_snowflake_query()
        print("Sleeping")
        # wait  10 Mins in between each thread
        sleep(100)
 
 
if __name__ == "__main__":
    thread = Thread(target = threaded_function)
    thread.start()
    thread.join()
    print("unexpected")