class Constants:
    PROJECT_PATH = 'C://Users//pankaj.verma//Downloads//Python-Framework//Python_Frmework//'
    SNOWFLAKE_SECTION = 'snowflakeSection'
    SF_USER = 'sfUser'
    SF_PASSWORD = 'sfPassword'
    SF_ACCOUNT = 'sfAccount'
    SF_DATABASE = 'sfDatabaseName'
    SF_SCHEMA = 'sfSchema'
    SF_WAREHOUSE = 'sfWareHouse'
    SF_ROLE = 'sfRole'
    SF_QUERY = 'sf.mainquery'
    SF_SUB_QUERY = 'sf.subquery'
    SF_FETCH_LOADTIME = 'sf.fetch.loadtime'
    SF_FETCH_MAX_LOADTIME = 'sf.fetch.max.loadtime'
    SF_UPDATE_LOADTIME = 'sf.update.loadtime'
    DBT_SECTION = 'dbtSection'
    DBT_ACCOUNT_ID = 'accountId'
    DBT_JOB_ID = 'jobId'
    DBT_API_KEY = 'apiKey'