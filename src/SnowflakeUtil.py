import snowflake.connector
from src.Constants import Constants
from src.ConfigReader import ConfigReader


config = ConfigReader("snowflake_config.properties").read_config()


class SnowflakeUtil:

    def get_snowflake_engine(self):

        sf_user = config.get(Constants.SNOWFLAKE_SECTION, Constants.SF_USER)
        sf_password = config.get(Constants.SNOWFLAKE_SECTION, Constants.SF_PASSWORD)
        sf_warehouse = config.get(Constants.SNOWFLAKE_SECTION, Constants.SF_WAREHOUSE)
        sf_account = config.get(Constants.SNOWFLAKE_SECTION, Constants.SF_ACCOUNT)
        sf_role = config.get(Constants.SNOWFLAKE_SECTION, Constants.SF_ROLE)
        sf_database_name = config.get(Constants.SNOWFLAKE_SECTION, Constants.SF_DATABASE)
        sf_schema = config.get(Constants.SNOWFLAKE_SECTION, Constants.SF_SCHEMA)

        try:
            conn = snowflake.connector.connect(
                user=sf_user,
                account=sf_account,
                warehouse=sf_warehouse,
                database=sf_database_name,
                password=sf_password,
                schema=sf_schema,
                role=sf_role
            )

            return conn
        except Exception as e:
            raise e