from ctypes import sizeof
from pickle import EMPTY_LIST
import datetime
from tokenize import triple_quoted
from src.SnowflakeUtil import SnowflakeUtil
from src.ConfigReader import ConfigReader
from src.Constants import Constants
from src.DbtJobRunStatus import run
config = ConfigReader("snowflake_config.properties").read_config()


class SnowflakeExecuter:

    def execute_snowflake_query(self):
        try:
            sf_util = SnowflakeUtil()
            sf_conn = sf_util.get_snowflake_engine()

            last_load_time = SnowflakeExecuter().fetch_last_load_time()[0]

            sf_query = config.get(Constants.SNOWFLAKE_SECTION, Constants.SF_QUERY)
            sf_query = sf_query.replace('<LAST_LOAD_TIME>', str(last_load_time))

            sf_sub_query = config.get(Constants.SNOWFLAKE_SECTION, Constants.SF_SUB_QUERY)


            sf_cursor = sf_conn.cursor()
            sf_cursor.execute(sf_query)
            sf_cursor.execute(sf_sub_query)

            max_last_load_time = SnowflakeExecuter().fetch_max_last_load_time()[0]
            if (max_last_load_time is None):
                max_last_load_time=last_load_time

            SnowflakeExecuter().update_last_load_time(max_last_load_time)

            #for columns in sf_cursor:
            #while True:
            #    time.sleep(5000)
            size=0
            for col in sf_cursor:
                size=size+1

            status='Success'
            data_found=False

            if( size > 0 ):

                if ( run() == 'Success'):
                    data_found=True
                else:
                    status='Failure'

            run_cursor = sf_conn.cursor()
            run_cursor.execute(f"INSERT INTO \"OSCAR\".\"DATA_VAULT_MODEL\".\"RUN_HISTORY\" values('{datetime.datetime.now()}',{data_found},'{status}')")
            

        except Exception as e:
            raise e

    def fetch_last_load_time(self):
        try:
            sf_util = SnowflakeUtil()
            sf_conn = sf_util.get_snowflake_engine()

            sf_fetch_load_time = config.get(Constants.SNOWFLAKE_SECTION, Constants.SF_FETCH_LOADTIME)

            load_time_cursor = sf_conn.cursor()
            load_time_cursor.execute(sf_fetch_load_time)
            last_load_time = []

            for Load_time in load_time_cursor:
                last_load_time.append(Load_time[0])

            return last_load_time

        except Exception as e:
            raise e

    def update_last_load_time(self, last_load_time):
        try:
            sf_util = SnowflakeUtil()
            sf_conn = sf_util.get_snowflake_engine()

            sf_update_load_time = config.get(Constants.SNOWFLAKE_SECTION, Constants.SF_UPDATE_LOADTIME)
            
            sf_update_load_time = sf_update_load_time.replace('<LAST_LOAD_TIME>', str(last_load_time))
            
            load_time_cursor = sf_conn.cursor()
            load_time_cursor.execute(sf_update_load_time)

            return True
        except Exception as e:
            raise e

    def fetch_max_last_load_time(self):
        try:
            sf_util = SnowflakeUtil()
            sf_conn = sf_util.get_snowflake_engine()

            sf_max_fetch_load_time = config.get(Constants.SNOWFLAKE_SECTION, Constants.SF_FETCH_MAX_LOADTIME)
            load_time_cursor = sf_conn.cursor()
            load_time_cursor.execute(sf_max_fetch_load_time)
            max_last_load_time = []

            for Load_time in load_time_cursor:
                max_last_load_time.append(Load_time[0])
            return max_last_load_time

        except Exception as e:
            raise e
