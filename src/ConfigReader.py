import configparser
import os
from src.Constants import Constants


class ConfigReader:

    def __init__(self, cfg_file):
        self.config = configparser.RawConfigParser()
        self.cfg_file = cfg_file

    def read_config(self):
        if self.cfg_file is not None:
            if os.path.exists(Constants.PROJECT_PATH + "config/" + self.cfg_file):
                self.config.read(Constants.PROJECT_PATH + "config/" + self.cfg_file)
                return self.config
            else:
                raise Exception("File name is missing for reading the configuration")
