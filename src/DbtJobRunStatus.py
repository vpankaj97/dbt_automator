import enum
import time
from src.ConfigReader import ConfigReader
from src.Constants import Constants
import requests

config = ConfigReader("dbt_config.properties").read_config()

ACCOUNT_ID = config.get(Constants.DBT_SECTION, Constants.DBT_ACCOUNT_ID)
JOB_ID = config.get(Constants.DBT_SECTION, Constants.DBT_JOB_ID)
API_KEY = config.get(Constants.DBT_SECTION, Constants.DBT_API_KEY)


class DbtJobRunStatus(enum.IntEnum):
    QUEUED = 1
    STARTING = 2
    RUNNING = 3
    SUCCESS = 10
    ERROR = 20
    CANCELLED = 30


def _trigger_job() -> int:
    res = requests.post(
        url=f"https://cloud.getdbt.com/api/v2/accounts/{ACCOUNT_ID}/jobs/{JOB_ID}/run/",
        headers={'Authorization': f"Token {API_KEY}"},
        json={
            # Optionally pass a description that can be viewed within the dbt Cloud API.
            # See the API docs for additional parameters that can be passed in,
            # including `schema_override`
            'cause': f"Triggered using Python Frame Work!",
        }
    )
    try:
        res.raise_for_status()
    except Exception as e:
        print(f"API token (last four): ...{API_KEY[-4:]}")
        raise e

    response_payload = res.json()
    return response_payload['data']['id']


def _get_job_run_status(job_run_id):
    res = requests.get(
        url=f"https://cloud.getdbt.com/api/v2/accounts/{ACCOUNT_ID}/runs/{job_run_id}/",
        headers={'Authorization': f"Token {API_KEY}"},
    )

    res.raise_for_status()
    response_payload = res.json()
    return response_payload['data']['status']


def run():
    job_run_id = _trigger_job()

    print(f"job_run_id = {job_run_id}")

    while True:
        time.sleep(5)

        status = _get_job_run_status(job_run_id)

        print(f"status = {status}")

        if status == DbtJobRunStatus.SUCCESS:
            return 'Success'
        elif status == DbtJobRunStatus.ERROR or status == DbtJobRunStatus.CANCELLED:
            return 'Failure'


